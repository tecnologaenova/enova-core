<?php

namespace Enova\Core\Implementation;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Gears\ClassFinder;

class CommandApplication extends Application {

    private $commandClasses = [];
    private $finder =null;

    public function __construct($composerInstance,array $namespaces, string $name = 'UNKNOWN', string $version = 'UNKNOWN') {
        $this->finder=new ClassFinder($composerInstance);
        $this->scanNamespaces($namespaces);
        parent::__construct($name, $version);
    }

    private function scanNamespaces(array $namespaces) {
        foreach ($namespaces as $namespace){
           $classes = $this->finder->namespace($namespace)->extends(Command::class)->search();
           foreach($classes as $fileName=>$class){
               $this->commandClasses[]=$class;
           }
        }
    }

    public function run(InputInterface $input = null, OutputInterface $output = null) {
        foreach ($this->commandClasses as $command) {
            $this->add(new $command());
        }
        parent::run($input, $output);
    }

}
