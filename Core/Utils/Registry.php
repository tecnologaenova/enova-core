<?php

namespace Enova\Core\Utils;

/**
 * Description of Registry
 *
 * @author rm
 */
class Registry {
    static private $registry=[];
    
    static public function set($key,&$value){
        if(!array_key_exists($key, self::$registry)){
            self::$registry[$key]=$value;
            return true;
        }else{
            return false;
        }
    }
    
    static public function exists($key){
        return array_key_exists($key, self::$registry);
    }
    
    static public function get($key){
        if(array_key_exists($key, self::$registry)){
            return self::$registry[$key];
        }else{
            return null;
        }
    }
    
    static public function remove($key){
        if(array_key_exists($key, self::$registry)){
            unset(self::$registry[$key]);
            return true;
        }else{
            return false;
        }
    }
    
    static function clear(){
        self::$registry=[];
    }
    
    static function printer(){
        print_r(self::$registry);
    }
}
