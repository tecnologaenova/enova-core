<?php

namespace Enova\Core\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Enova\Core\Utils\Registry;

//https://php-code-generator.readthedocs.io/en/latest/generator.html#codefilegenerator
//https://packagist.org/packages/nette/php-generator

class ModuleCreate extends Command {

    protected function configure() {
        $this
                ->addArgument('name', InputArgument::REQUIRED, 'Name of the Module to create')
                ->setName("module:create");
    }

    
    public function execute(InputInterface $input, OutputInterface $output) {
        $modulesPath = Registry::get('root.directory') . "/Modules";
        try {
            if (file_exists("{$modulesPath}/{$input->getArgument('name')}")) {
                throw new \Exception("The directory {$input->getArgument('name')} already exists!");
            }
            if (!mkdir("{$modulesPath}/{$input->getArgument('name')}")) {
                throw new \Exception("There was a problem creating the module directory : {$input->getArgument('name')}");
            }
            $this->createDirectoryStructure($modulesPath . "/" . $input->getArgument('name'));
            $output->writeln("<>The new module {$input->getArgument('name')} was created!");
        } catch (\Exception $ex) {
            $output->writeln("<error>".$ex->getMessage()."</error>");
        }
    }

    public function createDirectoryStructure($basePath) {
        $directories = [
            'Business',
            'Controller',
            'Exception',
            'Helper',
            'Model',
            'Seed'
        ];
        foreach ($directories as $dir) {
            if (!mkdir($basePath . "/" . $dir)) {
                throw new \Exception('There was a problem creating the directory: ' . $dir);
            }
        }
    }

}
